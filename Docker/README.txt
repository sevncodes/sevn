The folder contain the Dockerfile used to generate the images of the SEVN container.
The Makefile make easier to build and push the images in the DockerHub.

The images and a minimal documentation can be found at:
    - sevndocker: https://hub.docker.com/repository/docker/iogiul/sevndocker/general
    - sevnpydocker: https://hub.docker.com/repository/docker/iogiul/senvpydocker/general