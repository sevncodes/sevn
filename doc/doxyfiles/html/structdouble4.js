var structdouble4 =
[
    [ "double4", "structdouble4.html#a062dc4a6a74bb618c6a072a698080e69", null ],
    [ "double4", "structdouble4.html#a5bd8ec39dd5ff836d8f93fbb1e29ddd7", null ],
    [ "mod", "structdouble4.html#a24165b95acd74efe967eecef71c4a2ab", null ],
    [ "set_w", "structdouble4.html#ae42693db75f33ff299f41c8f202612be", null ],
    [ "set_x", "structdouble4.html#abe0dfa3d80539f596f1006cbd21d52d9", null ],
    [ "set_y", "structdouble4.html#a7b4f15272e8d10dffb7eaef92b122aed", null ],
    [ "set_z", "structdouble4.html#a4dde99be97084167ca4bb600cb8ecb84", null ],
    [ "w", "structdouble4.html#afda46a402e033a30c3803cbc10a60116", null ],
    [ "x", "structdouble4.html#ac67fc9d197545db6375b5200e0d67f94", null ],
    [ "y", "structdouble4.html#a70fe96016cd0dd72703afa11ffe52788", null ],
    [ "z", "structdouble4.html#a7943d8c026d4f47ad4872b5830009825", null ]
];