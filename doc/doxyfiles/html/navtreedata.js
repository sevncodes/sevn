/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "SEVN", "index.html", [
    [ "News", "index.html#autotoc_md0", null ],
    [ "Quickstart", "index.html#autotoc_md1", [
      [ "Requirements", "index.html#autotoc_md2", [
        [ "Conda environments", "index.html#autotoc_md3", null ]
      ] ],
      [ "Installation", "index.html#autotoc_md4", [
        [ "Compile script", "index.html#autotoc_md5", null ],
        [ "Cmake", "index.html#autotoc_md6", null ]
      ] ],
      [ "Run", "index.html#autotoc_md7", [
        [ "Run scripts", "index.html#autotoc_md8", null ]
      ] ]
    ] ],
    [ "Documentation", "index.html#autotoc_md9", null ],
    [ "Support", "index.html#autotoc_md10", null ],
    [ "Contributing", "index.html#autotoc_md11", null ],
    [ "Authors and acknowledgment", "index.html#autotoc_md12", null ],
    [ "License", "index.html#autotoc_md13", null ],
    [ "Developers guide", "md_devepguide.html", null ],
    [ "FAQ", "md__f_a_q.html", null ],
    [ "WIKI", "md__wiki.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_b_s_eintegrator_8h.html",
"class_b_j_i_t___property.html#a56b0e563f1deae887de4ed117914e346",
"class_b_worldtime.html#a6ee50b550adecd1e2961f401e4478198",
"class_c_c15.html#aaad1d0c62bd257c7515dcf6db8ee03f4",
"class_circularisation_periastron_full.html#a8af93dedd4c8c423005349fef53e17aa",
"class_core_radius.html#a30d86c7b7e739b1afc7a4d53cc9eea28",
"class_depthconv.html#a7c2a7b00c6288012f1a9d406cd05d22e",
"class_e_c_u_s30.html#a90ef65680bb5a84681b02b9939cecbe9",
"class_empty.html#a86f70b8787f98515ecef4505218ca863",
"class_g_wtime.html#a01bc98fce22cf5f8891536b64a77af85",
"class_hardening_fast_cluster.html#ad89d46b229414c2cd8077312b06e06cc",
"class_hurley__mod__rl.html#aa348c37d1e944257eab4d969d9529bab",
"class_hurley__rl__propeller.html#a3cf29e9a32155ff44d74e1313cda8d07",
"class_inertia.html#af2aa3f6a91b5136dd44db6c1016b1691",
"class_kollision_hurley.html#ae7a427bc9dc3e272a681da3820024f6c",
"class_localtime.html#aaf49297bba6c426e5470c6e69fd50c3a",
"class_m_h_e.html#adcc337246ac8bfabbf3bb52603b13710",
"class_mass__obejct.html#a7c2a7b00c6288012f1a9d406cd05d22e",
"class_n_srem.html#abe166432f487f7038776503c336f4788",
"class_nsup.html#a50ef56abe329da594c8cf7629f3243ce",
"class_optional_table_property.html#a4b6d00c711649c25853e88eb591b96ad",
"class_orbital__change___tides.html#ae7da17a3883f0e69463f6ebfc3f2bbc7",
"class_phase.html#a55bb13c2b1ee36882c549d9cf37e2df0",
"class_plife.html#a5496b2bc47beb1dc748353a358fae0fb",
"class_qconv.html#afaa31d37eacc8b18d57b7a681d08e062",
"class_r_c_o.html#a3b81e1e12799a9796befb8f4edcb0dd7",
"class_radius.html#a74dfe439b14301e6b98a2d10e716fdc9",
"class_rs.html#afc8dd335e4664b76a85e68e26d761dc4",
"class_standard_circularisation.html#a9f6f2ebf1bbd816f29816ec1d47ad321",
"class_star.html#ad2ae291f465e34f94209cce961283831",
"class_star__auxiliary.html#ac8525ede59e3fb232f204d61a7ad3f90",
"class_tconv.html#a16f2fd6eedb09bc1dbaf90d76cb1bcd2",
"class_tides__simple.html#aeda43fc356df43310dcd6c038f8cf21d",
"class_w_drem.html#a9b8ae00c6b853ca8420f2a6fa09950f8",
"class_worldtime.html#afb2f76dec040c60ff20fec8fa7437363",
"class_zombierem.html#a0d01bba3fe6e3d8fbfc969da3c18d3ef",
"classd_m_r_l_odt.html#a5496b2bc47beb1dc748353a358fae0fb",
"classd_mcumul__binary.html#ab0d11a6def61fa76b16cad9e3e765b30",
"classdadt.html#a432d31a2a688bd35266efcfe1b04e3cb",
"classdisabled___s_n_kicks.html#a3b9292523b895524738c31aa25601459",
"classevolve__utility_1_1_evolve_b_h_m_s.html#a2356fd448fb3b566eae418728ad961ed",
"classevolve__utility_1_1_evolve_functor.html#ad66d499476c18572ad91aa081f72565b",
"classrapid__gau_n_s.html#ac46c57101482039076c6264e36198e0c",
"classsimple__mix.html#a228001b052048ab2b0003550f3e07401",
"hobbs_8h.html",
"namespace_lookup.html#a4ab0d50e2efe08ff9b26efa1c1018ddb",
"namespaceutilities.html#ab6872b4710c09a5803080889cbde042b",
"utilities_8h.html#ab5a36637937008a46b2a3ed133c14607"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';