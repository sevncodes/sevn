var class_orbital__change___c_e =
[
    [ "Orbital_change_CE", "class_orbital__change___c_e.html#a700cde2a780c83ef6d79fe18fc248557", null ],
    [ "~Orbital_change_CE", "class_orbital__change___c_e.html#abc228f03095c9480d1939c86f62842e4", null ],
    [ "DA", "class_orbital__change___c_e.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "class_orbital__change___c_e.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_orbital__change___c_e.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "class_orbital__change___c_e.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "class_orbital__change___c_e.html#abcc3a1164cb1e983d9709cbb2f8a642d", null ],
    [ "GetUsed", "class_orbital__change___c_e.html#a8588efa9f0eab69bb15fa41c5b543aee", null ],
    [ "init", "class_orbital__change___c_e.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_orbital__change___c_e.html#a424e6cf05c7bda82367ec01a797a71a0", null ],
    [ "Instance", "class_orbital__change___c_e.html#a26f390cf2a7756ce996d70f77d086ff0", null ],
    [ "is_process_ongoing", "class_orbital__change___c_e.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_orbital__change___c_e.html#a04a901671b46b941809581dcf3c9fb70", null ],
    [ "Register", "class_orbital__change___c_e.html#a43de6b4f55e5dd91516ccde047ec6c99", null ],
    [ "set_options", "class_orbital__change___c_e.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_orbital__change___c_e.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "svlog", "class_orbital__change___c_e.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];