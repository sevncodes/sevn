var class_hurley__winds =
[
    [ "Hurley_winds", "class_hurley__winds.html#ac40ce53f1c083d8fb7007a69417d91e2", null ],
    [ "~Hurley_winds", "class_hurley__winds.html#a8c1326b53992289e51fa9fac957606db", null ],
    [ "DA", "class_hurley__winds.html#a04814a9cd985de0f31fc74011d0f1e56", null ],
    [ "DAngMomSpin", "class_hurley__winds.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_hurley__winds.html#a74abc8183287d06303cbf8dd5e6b3ffd", null ],
    [ "DM", "class_hurley__winds.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "class_hurley__winds.html#a2f8118fb28c939655f9a1dff47f73a3d", null ],
    [ "GetUsed", "class_hurley__winds.html#a167f5aff25188a471f3ce86e061fe9dc", null ],
    [ "init", "class_hurley__winds.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_hurley__winds.html#a3f5acce5accdfd67ac429e5be8c268e0", null ],
    [ "Instance", "class_hurley__winds.html#a9b10015bec8cf096e139260c5eee4f6b", null ],
    [ "is_process_ongoing", "class_hurley__winds.html#ad959a95d6918851c39ccb760d5591a39", null ],
    [ "name", "class_hurley__winds.html#a0b92eb66a525b0cf0eedb5b01a12b472", null ],
    [ "Register", "class_hurley__winds.html#aa8b054420b1078c9200866fdc5b79a1c", null ],
    [ "set_options", "class_hurley__winds.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_hurley__winds.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "_Hurley_winds", "class_hurley__winds.html#ad501f0900bd0985babec238597507c35", null ],
    [ "svlog", "class_hurley__winds.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];