var class_orbital__change___g_w =
[
    [ "Orbital_change_GW", "class_orbital__change___g_w.html#a22ad3f02bc689f8fd719d12c18b68736", null ],
    [ "~Orbital_change_GW", "class_orbital__change___g_w.html#a110ad2c4961e10cfc1940775dbf27fb7", null ],
    [ "DA", "class_orbital__change___g_w.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "class_orbital__change___g_w.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "class_orbital__change___g_w.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "class_orbital__change___g_w.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "GetStaticMap", "class_orbital__change___g_w.html#a82ce4b3e4b18d1dcd9d56d58573c2898", null ],
    [ "GetUsed", "class_orbital__change___g_w.html#aa062cc01e0f5126b37179f2f9554ac94", null ],
    [ "init", "class_orbital__change___g_w.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "class_orbital__change___g_w.html#a87075308a49a3d53f4f311e8b74f987b", null ],
    [ "Instance", "class_orbital__change___g_w.html#abfc6b84011ce3478d9a5b7c511047ba2", null ],
    [ "is_process_ongoing", "class_orbital__change___g_w.html#a9b10dd053f3b74652cfad5f58064c0e1", null ],
    [ "name", "class_orbital__change___g_w.html#ab3f5dd577272b6905472225f4943f0e7", null ],
    [ "Register", "class_orbital__change___g_w.html#aa4be476e5a40e9a68415ae915b0a7f8a", null ],
    [ "set_options", "class_orbital__change___g_w.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "speciale_evolve", "class_orbital__change___g_w.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "svlog", "class_orbital__change___g_w.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ]
];