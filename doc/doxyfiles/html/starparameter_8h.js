var starparameter_8h =
[
    [ "PROC_ID", "starparameter_8h.html#a0b955e643744ae45bcc6bcd3ed9c3ae3", [
      [ "CE", "starparameter_8h.html#a0b955e643744ae45bcc6bcd3ed9c3ae3a7dc5845e3ab76dd0372d5a4363fbce45", null ],
      [ "RL", "starparameter_8h.html#a0b955e643744ae45bcc6bcd3ed9c3ae3a5716cbca66469410f40afdf172c3a49e", null ],
      [ "TD", "starparameter_8h.html#a0b955e643744ae45bcc6bcd3ed9c3ae3abb4b3ab6b830f99333dd7236be0d5b41", null ],
      [ "COUNT", "starparameter_8h.html#a0b955e643744ae45bcc6bcd3ed9c3ae3ad886e1cd554cb3d829ab7e0c3bd26550", null ]
    ] ],
    [ "MAX_Z", "starparameter_8h.html#ab8be0ff22f1e954e505b44689f594355", null ],
    [ "MAX_ZAMS", "starparameter_8h.html#a9449c230af43923de4645f50b8e8eb07", null ],
    [ "maximum_variation", "starparameter_8h.html#a8398de803188e71e7b07a21cd0c78d0e", null ],
    [ "min_points_per_phase", "starparameter_8h.html#a493358f1101d4a20faf2e5cf358d1800", null ],
    [ "MIN_Z", "starparameter_8h.html#af3d3b9d2b06bbc857a2713b533f2dfae", null ],
    [ "MIN_ZAMS", "starparameter_8h.html#a061b6f4ce330354a6dc5aad844ada898", null ],
    [ "semimajor", "starparameter_8h.html#a1f3edb108ff96addb6f14580dc6980db", null ],
    [ "wrtolerance", "starparameter_8h.html#aabb9aa2c9f59d282a631269057156d47", null ]
];