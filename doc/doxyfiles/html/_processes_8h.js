var _processes_8h =
[
    [ "Process", "class_process.html", "class_process" ],
    [ "MaccretionProcess", "class_maccretion_process.html", "class_maccretion_process" ],
    [ "RocheLobe", "class_roche_lobe.html", "class_roche_lobe" ],
    [ "CommonEnvelope", "class_common_envelope.html", "class_common_envelope" ],
    [ "SNKicks", "class_s_n_kicks.html", "class_s_n_kicks" ],
    [ "GWrad", "class_g_wrad.html", "class_g_wrad" ],
    [ "Tides", "class_tides.html", "class_tides" ],
    [ "Mix", "class_mix.html", "class_mix" ]
];