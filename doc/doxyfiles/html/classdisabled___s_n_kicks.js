var classdisabled___s_n_kicks =
[
    [ "disabled_SNKicks", "classdisabled___s_n_kicks.html#abf559f0b1a5813ab4d783bfa8c598d9c", null ],
    [ "DA", "classdisabled___s_n_kicks.html#ae7d7e0628e5047a3d810380afb079e14", null ],
    [ "DAngMomSpin", "classdisabled___s_n_kicks.html#accdda03f7f2e38feae3995ea9b2cdb6c", null ],
    [ "DE", "classdisabled___s_n_kicks.html#adc54ac8c6290228dcec5d6dfdb9eec51", null ],
    [ "DM", "classdisabled___s_n_kicks.html#a802983c4798625dcb0319be0b971695d", null ],
    [ "get_cos_nu", "classdisabled___s_n_kicks.html#a3d392dd57fcfa5422b9ddf87047449cc", null ],
    [ "get_vcom", "classdisabled___s_n_kicks.html#a97b2c2d6b409f7dd459abbceafcdb409", null ],
    [ "GetStaticMap", "classdisabled___s_n_kicks.html#a9c475ba159d680bad633ba7a9c5038e8", null ],
    [ "GetUsed", "classdisabled___s_n_kicks.html#ac27b749920e0d1d4ba76ace593401655", null ],
    [ "init", "classdisabled___s_n_kicks.html#adeb58a2c147ad895d313159a59b24487", null ],
    [ "instance", "classdisabled___s_n_kicks.html#af556e4234f89c0b2d28f70fcb816b238", null ],
    [ "Instance", "classdisabled___s_n_kicks.html#a1af9e4a9fb5a167aca75940e6c16fa4c", null ],
    [ "is_process_ongoing", "classdisabled___s_n_kicks.html#a0bbf3d0d404cbc320e22b1f7be3b64f6", null ],
    [ "name", "classdisabled___s_n_kicks.html#a3811e2eabbb049039cf5254e2fb69d80", null ],
    [ "Register", "classdisabled___s_n_kicks.html#ad2c47390cdd35bae409e8bbf6858ccab", null ],
    [ "set_cos_nu", "classdisabled___s_n_kicks.html#a1aef52a56d835603c1edb380b8649e11", null ],
    [ "set_options", "classdisabled___s_n_kicks.html#ab647fbd4fa38d29098cd8e0173391b54", null ],
    [ "set_vcom", "classdisabled___s_n_kicks.html#a830b76b08add5a01d759a4b453c1bd33", null ],
    [ "speciale_evolve", "classdisabled___s_n_kicks.html#a3425194ee18f327bbe9751aaca12df1b", null ],
    [ "_disabled_SNKicks", "classdisabled___s_n_kicks.html#a3b9292523b895524738c31aa25601459", null ],
    [ "cos_nu", "classdisabled___s_n_kicks.html#ad611fcf6e02c9a62b76d1d1edfce2ef8", null ],
    [ "svlog", "classdisabled___s_n_kicks.html#a42a991d78f0cdd3bc7cfbd79bf57e9b0", null ],
    [ "vcom", "classdisabled___s_n_kicks.html#a4977c0e6bcc7467b38c14242530e75ac", null ]
];