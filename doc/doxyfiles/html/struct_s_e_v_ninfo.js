var struct_s_e_v_ninfo =
[
    [ "get_full_info", "struct_s_e_v_ninfo.html#a6cf593ce671e14010e8dd89fd828e1d5", null ],
    [ "DEVLINE", "struct_s_e_v_ninfo.html#adf6689e524df06df64f798adaab2db8a", null ],
    [ "GIT_BRANCH", "struct_s_e_v_ninfo.html#a6d57c13bb0d996afbfe23293fcdeb6bb", null ],
    [ "GIT_COUNTER", "struct_s_e_v_ninfo.html#aa4708583ea1552ebee94263223171d6c", null ],
    [ "GIT_COUNTER_REMOTE", "struct_s_e_v_ninfo.html#ad2e5960dc9406e26ed6e2ad584ed0f5b", null ],
    [ "GIT_SHA", "struct_s_e_v_ninfo.html#ad25634c53dc4f10321ab897066bafd00", null ],
    [ "GIT_SHA_REMOTE", "struct_s_e_v_ninfo.html#a83f48ca0f1f6bce99916235b8899bb35", null ],
    [ "GIT_SHATIME", "struct_s_e_v_ninfo.html#a56ace2ecf11bc199f7bfb9339bd70e8d", null ],
    [ "GIT_SHATIME_REMOTE", "struct_s_e_v_ninfo.html#a7668a62fe83683320533c7bc41a26458", null ],
    [ "GIT_SHATIMESTAMP", "struct_s_e_v_ninfo.html#a67a8489385ffda64600a4c5611d6d687", null ],
    [ "GIT_SHATIMESTAMP_REMOTE", "struct_s_e_v_ninfo.html#a0f61b57c8b6ffea5cf00f641e88f25ee", null ],
    [ "VERSION", "struct_s_e_v_ninfo.html#ac767385a7bcf325d077e0ddf3d2c1b1c", null ],
    [ "VERSION_MAJOR", "struct_s_e_v_ninfo.html#a7deed9c30fa7559c098736c34e83b113", null ],
    [ "VERSION_MINOR", "struct_s_e_v_ninfo.html#a6b681ddd2525691eb05adb5ea10ebe9c", null ],
    [ "VERSION_PATCH", "struct_s_e_v_ninfo.html#ae57790dc52c82b7b48a287b17848512c", null ]
];