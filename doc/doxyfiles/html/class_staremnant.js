var class_staremnant =
[
    [ "Staremnant", "class_staremnant.html#a649ad8a48f22dc310879d76bae550dbc", null ],
    [ "Staremnant", "class_staremnant.html#aac947b5fe9a2e4e2aa1c3a5c261d7cf4", null ],
    [ "~Staremnant", "class_staremnant.html#a0d3d4dc053805c077a846fa19af28182", null ],
    [ "age", "class_staremnant.html#a81d90357225dd7b0e189413347733e8e", null ],
    [ "Bmag", "class_staremnant.html#abec12f904fe531b25c72cce23d7e51a8", null ],
    [ "get", "class_staremnant.html#a4f0a63fddb7ee138f5c494af44132312", null ],
    [ "get_born_time", "class_staremnant.html#a3fb2eec3b62247ddec79d9256b3f6dfd", null ],
    [ "get_Mremnant_at_born", "class_staremnant.html#ac7592dbfe124d980ffa2589c23a54649", null ],
    [ "get_remnant_type", "class_staremnant.html#af172e833f7a2be034177e5ec3546760c", null ],
    [ "Inertia", "class_staremnant.html#ad96a80f4c61443108dd8d15fb94e5295", null ],
    [ "InertiaSphere", "class_staremnant.html#ab1a74d09d4b48efdddce170bbd06c1f8", null ],
    [ "Luminosity", "class_staremnant.html#a2b435815f42c861010d73a9bca4112e4", null ],
    [ "Mass", "class_staremnant.html#ae1217a0b00f428114665550c1acfaa23", null ],
    [ "OmegaRem", "class_staremnant.html#a3277d5ecf9f12c888190129c083b030a", null ],
    [ "Radius", "class_staremnant.html#ad9f236c20b73c9b786d2e7c135a2d219", null ],
    [ "Xspin", "class_staremnant.html#a00a2981c7327990d34b58b01eb493787", null ],
    [ "born_time", "class_staremnant.html#a9b8ae00c6b853ca8420f2a6fa09950f8", null ],
    [ "Mremnant_at_born", "class_staremnant.html#a15460ef0ce7a97c3eea8b159ea9ff94a", null ],
    [ "remnant_type", "class_staremnant.html#a1f2ee7c0a66aabc56e7f038fcc97e033", null ],
    [ "svlog", "class_staremnant.html#a5d7579476b76d081c413d00b07595c67", null ]
];