var namespacesevnstd =
[
    [ "bse_error", "classsevnstd_1_1bse__error.html", "classsevnstd_1_1bse__error" ],
    [ "ce_error", "classsevnstd_1_1ce__error.html", "classsevnstd_1_1ce__error" ],
    [ "jtrack_error", "classsevnstd_1_1jtrack__error.html", "classsevnstd_1_1jtrack__error" ],
    [ "notimplemented_error", "classsevnstd_1_1notimplemented__error.html", "classsevnstd_1_1notimplemented__error" ],
    [ "params_error", "classsevnstd_1_1params__error.html", "classsevnstd_1_1params__error" ],
    [ "rl_error", "classsevnstd_1_1rl__error.html", "classsevnstd_1_1rl__error" ],
    [ "sanity_error", "classsevnstd_1_1sanity__error.html", "classsevnstd_1_1sanity__error" ],
    [ "sevnerr", "classsevnstd_1_1sevnerr.html", "classsevnstd_1_1sevnerr" ],
    [ "sevnio_error", "classsevnstd_1_1sevnio__error.html", "classsevnstd_1_1sevnio__error" ],
    [ "SevnLogging", "classsevnstd_1_1_sevn_logging.html", "classsevnstd_1_1_sevn_logging" ],
    [ "sn_error", "classsevnstd_1_1sn__error.html", "classsevnstd_1_1sn__error" ],
    [ "sse_error", "classsevnstd_1_1sse__error.html", "classsevnstd_1_1sse__error" ]
];