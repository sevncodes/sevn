var struct_qcrit =
[
    [ "Qcrit", "struct_qcrit.html#a1657c39bf473739eac8029a544f5447b", null ],
    [ "Qcrit", "struct_qcrit.html#ac3ace9aaeae9822d937a91e9c3daff85", null ],
    [ "_Hurley_giant", "struct_qcrit.html#a6ae7dc088a121e01aab4d8e2487b3545", null ],
    [ "_Webbink_giant", "struct_qcrit.html#a7988e38b46a07ba8a69bfbea61503a7c", null ],
    [ "qcrit_COMPAS", "struct_qcrit.html#a4fc383a8de18dc4b87ca263cb8d3e5b5", null ],
    [ "qcrit_constant", "struct_qcrit.html#af3105a7d887acbf3d1966698df961c57", null ],
    [ "qcrit_COSMIC_Claeys", "struct_qcrit.html#ab7121c61f346285cfb71cef3f94af9c1", null ],
    [ "qcrit_Hurley", "struct_qcrit.html#a6077c30b6365d621b7550ba94f194c40", null ],
    [ "qcrit_Hurley_Webbink", "struct_qcrit.html#a025bdd97552ef97cd2b3a7e6d626f88e", null ],
    [ "qcrit_Hurley_Webbink_Shao", "struct_qcrit.html#aea44cb64a4083037c73a9bf1bc1d721c", null ],
    [ "qcrit_Neijssel", "struct_qcrit.html#a9b208a8e8757acafd51f03e497b5546b", null ],
    [ "qc_const", "struct_qcrit.html#a558d74d26ec341e695f6d4146979e57d", null ]
];