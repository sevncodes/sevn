var class_thook =
[
    [ "Thook", "class_thook.html#adff84ba452cfe6c6364fc545735b37b4", null ],
    [ "eval", "class_thook.html#a796ffa171b3c4fbf2c5a41078ccc1444", null ],
    [ "mu", "class_thook.html#aa1f07eed0b4a88df80403ded97eedd42", null ],
    [ "operator()", "class_thook.html#aa6826efd4e81ea51de46c02ba3336cf0", null ],
    [ "units", "class_thook.html#a74f74d626cc180a06366b33d557aae02", null ],
    [ "a1", "class_thook.html#aa19b3ac98db058fbc7ab8d3cb84c8218", null ],
    [ "a10", "class_thook.html#a4e239a49e6fac6adb24d75e43ef372c1", null ],
    [ "a2", "class_thook.html#ad29a4d74932f4a3e38561b9c8f5a44bc", null ],
    [ "a3", "class_thook.html#aa26e773cc561edbc83e44d61a67cac52", null ],
    [ "a4", "class_thook.html#a24e00ae531b2af2b88593616514b2056", null ],
    [ "a5", "class_thook.html#a54bf441a653ecee87ee24b5d6c398d7a", null ],
    [ "a6", "class_thook.html#a15ccd0ec3a0a290fd6fefe9037ce0f12", null ],
    [ "a7", "class_thook.html#a315544610b21eee36a042cd68925b151", null ],
    [ "a8", "class_thook.html#a1a46ba650c1b9dbb30f1fe21e43172fe", null ],
    [ "a9", "class_thook.html#adbbbd3fc50fb4e79c9d1230c5f9e49aa", null ],
    [ "chaced_logP", "class_thook.html#ab1e7143a7fd9cfd10eaceaaf2e8edf02", null ],
    [ "chaced_M", "class_thook.html#a0cf80f56c1319bd78579e7a1aa94ceef", null ],
    [ "coeff", "class_thook.html#a22c53ea104f6abdd1141ca332aea4f4d", null ],
    [ "Z", "class_thook.html#add15815038ec1bed19d066ccb1379d64", null ]
];