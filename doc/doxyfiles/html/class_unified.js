var class_unified =
[
    [ "Unified", "class_unified.html#a3e481869b0eee53a7f5024788a0105ee", null ],
    [ "_apply", "class_unified.html#a5574ee61f5450621cbbf4b851aadc319", null ],
    [ "_apply", "class_unified.html#a7cfe0e6e8cc51ea1160ef4914539e356", null ],
    [ "apply", "class_unified.html#a24df9d565af3b8a2c37575cfd152e561", null ],
    [ "check_and_correct_vkick", "class_unified.html#ad579aeb54a9bc522bc03b7afd3229525", null ],
    [ "draw_from_gaussian", "class_unified.html#abcdf2feb02d48058575eb7fa1ce19b51", null ],
    [ "get_random_kick", "class_unified.html#a8f2cbe6992b73bd4909aecefa857a09a", null ],
    [ "GetStaticMap", "class_unified.html#add42458ac89c18dd5b4ccdb572aec0e0", null ],
    [ "GetUsed", "class_unified.html#a8af03dddc996c29395b57e0b41abf2f8", null ],
    [ "instance", "class_unified.html#a3541bdc65b6c7d53986c3251f8cbb123", null ],
    [ "Instance", "class_unified.html#a0bcf9d165b436dceef7b3636600f9750", null ],
    [ "kick_initializer", "class_unified.html#ad1d51caa463f9268cbf1c349e3ee05a0", null ],
    [ "name", "class_unified.html#a22b36ddac6404a0d54fcf4c46fd24b80", null ],
    [ "Register", "class_unified.html#a86dff21954db932117e2b4ff1146a192", null ],
    [ "set_random_kick", "class_unified.html#a29029dc8666af30104c1483c60c1ac7e", null ],
    [ "_unified", "class_unified.html#a5c22201ed462d2e916c42f2f15d61bc7", null ],
    [ "random_velocity_kick", "class_unified.html#aa489fda64b125b3465f9f6249de25b83", null ],
    [ "standard_gaussian", "class_unified.html#ae1056779a3fc3a09c414618b5c61eb92", null ],
    [ "svlog", "class_unified.html#a90ef65680bb5a84681b02b9939cecbe9", null ],
    [ "uniformRealDistribution", "class_unified.html#af209b3d8f2caa3fc991c5f53b6f1f0d3", null ]
];