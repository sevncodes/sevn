var class_hobbs =
[
    [ "Hobbs", "class_hobbs.html#a8348f518eb0792ce988fe42c0cf47655", null ],
    [ "_apply", "class_hobbs.html#a5574ee61f5450621cbbf4b851aadc319", null ],
    [ "_apply", "class_hobbs.html#ac00446332eca7340f3b241da88e0ba33", null ],
    [ "apply", "class_hobbs.html#a24df9d565af3b8a2c37575cfd152e561", null ],
    [ "check_and_correct_vkick", "class_hobbs.html#ad579aeb54a9bc522bc03b7afd3229525", null ],
    [ "draw_from_gaussian", "class_hobbs.html#abcdf2feb02d48058575eb7fa1ce19b51", null ],
    [ "estimate_fallback_correction", "class_hobbs.html#a978a6ef77e31c7ee09e8e929d69fc323", null ],
    [ "get_random_kick", "class_hobbs.html#a8f2cbe6992b73bd4909aecefa857a09a", null ],
    [ "GetStaticMap", "class_hobbs.html#add42458ac89c18dd5b4ccdb572aec0e0", null ],
    [ "GetUsed", "class_hobbs.html#a8af03dddc996c29395b57e0b41abf2f8", null ],
    [ "instance", "class_hobbs.html#a13ad010ae7563115c8a3408df6447663", null ],
    [ "Instance", "class_hobbs.html#a0bcf9d165b436dceef7b3636600f9750", null ],
    [ "kick_initializer", "class_hobbs.html#ad1d51caa463f9268cbf1c349e3ee05a0", null ],
    [ "name", "class_hobbs.html#a40d68cfaeb5e824a993cd9d9c84e4040", null ],
    [ "Register", "class_hobbs.html#a86dff21954db932117e2b4ff1146a192", null ],
    [ "set_random_kick", "class_hobbs.html#a29029dc8666af30104c1483c60c1ac7e", null ],
    [ "_hobbs", "class_hobbs.html#aba6b4029368eeaf5d4f91f6057330e39", null ],
    [ "random_velocity_kick", "class_hobbs.html#aa489fda64b125b3465f9f6249de25b83", null ],
    [ "standard_gaussian", "class_hobbs.html#ae1056779a3fc3a09c414618b5c61eb92", null ],
    [ "svlog", "class_hobbs.html#a90ef65680bb5a84681b02b9939cecbe9", null ],
    [ "uniformRealDistribution", "class_hobbs.html#af209b3d8f2caa3fc991c5f53b6f1f0d3", null ]
];