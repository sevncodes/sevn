var class_kicks =
[
    [ "Kicks", "class_kicks.html#a057b89e2b95508404e47bf6311b94b5c", null ],
    [ "~Kicks", "class_kicks.html#a47709eb3642db98471c75b22cc86cc2f", null ],
    [ "_apply", "class_kicks.html#a5574ee61f5450621cbbf4b851aadc319", null ],
    [ "apply", "class_kicks.html#a24df9d565af3b8a2c37575cfd152e561", null ],
    [ "check_and_correct_vkick", "class_kicks.html#ad579aeb54a9bc522bc03b7afd3229525", null ],
    [ "draw_from_gaussian", "class_kicks.html#abcdf2feb02d48058575eb7fa1ce19b51", null ],
    [ "get_random_kick", "class_kicks.html#a8f2cbe6992b73bd4909aecefa857a09a", null ],
    [ "GetStaticMap", "class_kicks.html#add42458ac89c18dd5b4ccdb572aec0e0", null ],
    [ "GetUsed", "class_kicks.html#a8af03dddc996c29395b57e0b41abf2f8", null ],
    [ "instance", "class_kicks.html#ad55608716d3198c9d3c24f55e8b9b552", null ],
    [ "Instance", "class_kicks.html#a0bcf9d165b436dceef7b3636600f9750", null ],
    [ "kick_initializer", "class_kicks.html#ad1d51caa463f9268cbf1c349e3ee05a0", null ],
    [ "name", "class_kicks.html#ae66e86ba3f650fbc4e5db1ad86b60164", null ],
    [ "Register", "class_kicks.html#a86dff21954db932117e2b4ff1146a192", null ],
    [ "set_random_kick", "class_kicks.html#a29029dc8666af30104c1483c60c1ac7e", null ],
    [ "random_velocity_kick", "class_kicks.html#aa489fda64b125b3465f9f6249de25b83", null ],
    [ "standard_gaussian", "class_kicks.html#ae1056779a3fc3a09c414618b5c61eb92", null ],
    [ "svlog", "class_kicks.html#a90ef65680bb5a84681b02b9939cecbe9", null ],
    [ "uniformRealDistribution", "class_kicks.html#af209b3d8f2caa3fc991c5f53b6f1f0d3", null ]
];