var errhand_8h =
[
    [ "sevnstd::sevnerr", "classsevnstd_1_1sevnerr.html", "classsevnstd_1_1sevnerr" ],
    [ "sevnstd::sevnio_error", "classsevnstd_1_1sevnio__error.html", "classsevnstd_1_1sevnio__error" ],
    [ "sevnstd::sse_error", "classsevnstd_1_1sse__error.html", "classsevnstd_1_1sse__error" ],
    [ "sevnstd::bse_error", "classsevnstd_1_1bse__error.html", "classsevnstd_1_1bse__error" ],
    [ "sevnstd::rl_error", "classsevnstd_1_1rl__error.html", "classsevnstd_1_1rl__error" ],
    [ "sevnstd::ce_error", "classsevnstd_1_1ce__error.html", "classsevnstd_1_1ce__error" ],
    [ "sevnstd::sn_error", "classsevnstd_1_1sn__error.html", "classsevnstd_1_1sn__error" ],
    [ "sevnstd::jtrack_error", "classsevnstd_1_1jtrack__error.html", "classsevnstd_1_1jtrack__error" ],
    [ "sevnstd::params_error", "classsevnstd_1_1params__error.html", "classsevnstd_1_1params__error" ],
    [ "sevnstd::notimplemented_error", "classsevnstd_1_1notimplemented__error.html", "classsevnstd_1_1notimplemented__error" ],
    [ "sevnstd::sanity_error", "classsevnstd_1_1sanity__error.html", "classsevnstd_1_1sanity__error" ]
];