\label{index_md__Users_giulianoiorio_Documents_sevn_public_README}%
\Hypertarget{index_md__Users_giulianoiorio_Documents_sevn_public_README}%
 

Website\+: \href{https://sevncodes.gitlab.io/sevn/index.html}{\texttt{ https\+://sevncodes.\+gitlab.\+io/sevn/index.\+html}}

SEVN (Stellar EVolution for 𝑁-\/body) is a rapid binary population synthesis code. It gets as input the initial conditions of stars or binaries (masses, spin, semi-\/major axis, eccentricity) and evolve them. SEVN calculates stellar evolution by interpolating pre-\/computed sets of stellar tracks. Binary evolution is implemented by means of analytic and semi-\/analytic prescriptions. The main advantage of this strategy is that it makes the implementation more general and flexible\+: the stellar evolution models adopted in sevn can easily be changed or updated just by loading a new set of look-\/up tables. SEVN allows to choose the stellar tables at runtime, without modifying the internal structure of the code or even recompiling it.

SEVN is written entirely in C++ (without external dependencies) following the object-\/oriented programming paradigm. SEVN exploits the CPU-\/parallelisation through Open\+MP. The repository contains also SEVNpy, a Python companion module can be used to easily access the SEVN backend in Python.

Additional information on the technical details of SEVN can be found in the presentation paper (\href{https://ui.adsabs.harvard.edu/abs/2022arXiv221111774I/abstract}{\texttt{ Iorio et al., 2022}}, see also \href{https://ui.adsabs.harvard.edu/abs/2019MNRAS.485..889S/abstract}{\texttt{ Spera et al., 2019}}) and in the \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}}.\hypertarget{index_autotoc_md0}{}\doxysection{News}\label{index_autotoc_md0}
{\bfseries{\mbox{[}30-\/05-\/24\mbox{]} Version 2.\+10.\+0 released}}

Main changes\+:
\begin{DoxyItemize}
\item Main SEVN behaviour for star flagged as Wolf-\/\+Rayet has changed (the flagging conditions are set by the runtime parameter {\itshape star\+\_\+tshold\+\_\+\+WR\+\_\+envelope}). In the previous versions as soon as a star satisfied the conditions it was turned into a pure\+He. Now this option is not anymore the standard, but it can be turned on with the new runtime option {\itshape turn\+\_\+\+WR\+\_\+to\+\_\+pure\+He} (false by default).
\item Added the runtime parameter {\itshape check\+\_\+stalling\+\_\+time} to set the number of second to wait before to flag a system as stalling (default 5s). (This was introduced in V 2.\+9.\+0).
\item Improved the stability of the adaptive timestep when also the \mbox{\hyperlink{class_star}{Star}} spin is checked.
\item The lambda prescriptions by Klencki+21 ({\itshape star\+\_\+lambda=-\/4} and {\itshape star\+\_\+lambda=-\/41}) revised. We solved a bug for which all the stars were considered with solar metallicity, and we improve the treatment when the \mbox{\hyperlink{class_radius}{Radius}} of the star is outside the range used by Klencki+21 to define the fitting equations (see \href{https://gitlab.com/sevncodes/sevn/-/issues/4}{\texttt{ issue4}}).
\end{DoxyItemize}

{\bfseries{\mbox{[}16-\/04-\/24\mbox{]} Version 2.\+8.\+0 released}}

The main change with respect to the other minor versions is that we changed the Binary kick processes so that the SN kick (vkick in class \mbox{\hyperlink{class_star}{Star}}) remains always with the same units (km/s). Before this versione they were changed to Rsun/yr after a SN explosion in a binary.

{\bfseries{\mbox{[}24-\/03-\/24\mbox{]} Bugs fixed!}}

The current version contains fixes on minor bugs. The only one that deserve a mention is a bug on the estimate of the common envelope lambda with the fitting equation by \href{https://ui.adsabs.harvard.edu/abs/2021A\%26A...645A..54K/abstract}{\texttt{ Klencki+21}}, i.\+e. when the option {\itshape star\+\_\+lambda -\/4} is used. {\bfseries{All the other options are not impacted by this bug.}}

Anyway, during the bug fixing, we found that when massive (\texorpdfstring{$>$}{>}30 Msun) and metal-\/rich (Z/\+Zsun\texorpdfstring{$>$}{>}0.\+4) stars contract toward the end of their life, the lambda estimate (with option {\itshape star\+\_\+lambda -\/4} and {\itshape star\+\_\+lambda -\/41}) by enters in a regime of extrapolations with respect the domain studied in \href{https://ui.adsabs.harvard.edu/abs/2021A\%26A...645A..54K/abstract}{\texttt{ Klencki+21}}. As a consequence, the lambda becomes very small producing (likely) unphysical very large binding energies (\texorpdfstring{$>$}{>}1E52 ergs). We are working on finding the best solutions (see \href{https://gitlab.com/sevncodes/sevn/-/issues/4}{\texttt{ Issue\#4}}), {\bfseries{meanwhile, we suggest to not use the options {\itshape star\+\_\+lambda -\/4} and {\itshape star\+\_\+lambda -\/41} for production runs.}}

{\bfseries{\mbox{[}29-\/08-\/23\mbox{]} New development line released}}

The new SEVN development line {\itshape zelaous\+\_\+redgiant} has been publicly released. The old development line {\itshape humble\+\_\+yellowdwarf} is still available at this \href{https://gitlab.com/sevncodes/sevn/-/tree/SEVN_humble_yellowdwarf?ref_type=heads}{\texttt{ link}}.

The most import novelties are\+:


\begin{DoxyItemize}
\item The release of the first version of the Python companion module SEVNpy. It is included in the SEVN release. SEVNpy contains both utilities to analyse the outputs of the SEVN runs and class and methods to directly access the SEVN backend and evolve star and binary with the same performance of the SEVN C++ executables, but with all the flexibility of interpreted code such as Python. See the SEVNpy section in the userguide \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}} and the online documentation at \href{http://sevn.rtfd.io/}{\texttt{ http\+://sevn.\+rtfd.\+io/}}.
\item SEVN Docker image. We added a Docker container including a working version of SEVN in the \href{https://hub.docker.com}{\texttt{ Dockerhub}}. The are two images\+: \href{https://hub.docker.com/r/iogiul/sevndocker}{\texttt{ sevndocker}} including only a compiled version of SEVN in a Linux environment, and \href{https://hub.docker.com/r/iogiul/sevnpydocker}{\texttt{ sevnpydocker}} including also a basic Python3 installation including SEVNpy and a running Jupyter notebook that can be accessed by the local browser. The Docker images can be run in essentially any operative systems without caring about installation and dependencies (you need just to have a working version of Docker). See the Docker section in the \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}} for additional details and instructions.
\item Added a complete SEVN versioning policy. The version is specified in the format X.\+Y.\+Z, where X is the major version and will change only after a substantial refactoring of the code or after a significant change of the main SEVN algorithms. Y is the minor version and will change after changes that can break the compatibility with analysis tools or change of the input, addition of new input parameters or moderate refactoring. The Z is the patch version and change almost after each commit. Moreover, we include also the concept of \char`\"{}development\+\_\+line\char`\"{}, a development line can be considered a development branch that start to deviate from the main public branch for a variety of reason. When the variation are too divergent with respect to the current main branch, we create a new development\+\_\+line. Not all the development line will reach the status of new public branch. In order to get the info about the SEVN version, just run the SEVN executables without runtime parameters. This call will return the list of SEVN parameters with their default value and the info about the SEVN version.
\end{DoxyItemize}\hypertarget{index_autotoc_md1}{}\doxysection{Quickstart}\label{index_autotoc_md1}
\hypertarget{index_autotoc_md2}{}\doxysubsection{Requirements}\label{index_autotoc_md2}
In order to install and use SEVN
\begin{DoxyItemize}
\item C++ compiler (std C++14, but it is compatible with older compilers)
\begin{DoxyItemize}
\item SEVN can be compiled with any GNU C++ compiler, but a version \texorpdfstring{$>$}{>}4.\+8 is warmly suggested.
\item SEVN works with the intel C++ compilers from the \href{https://www.intel.com/content/www/us/en/developer/tools/oneapi/toolkits.html\#gs.ifxtu2}{\texttt{ Intel One api toolkit}}
\item SEVN can be compiled with clang (the default compiler in mac\+Os systems). Notice that the default clang compiler on Macs does not support Open\+MP, therefore it is not possible to exploit the SEVN parallelisation.
\end{DoxyItemize}
\item Cmake version \texorpdfstring{$>$}{>} 2.\+8 (\texorpdfstring{$>$}{>}3.\+2 is suggested)
\end{DoxyItemize}

SEVN uses Open\+MP to parallelise the runs, if the Cmake compiler will not find a working Open\+MP version (check the Cmake log), SEVN will run only in the serial mode.\hypertarget{index_autotoc_md3}{}\doxysubsubsection{Conda environments}\label{index_autotoc_md3}
We provide two conda environments that already fulfill the SEVN requirements. They are both located in the folder \+\_\+resources/\+:


\begin{DoxyItemize}
\item \href{https://gitlab.com/sevncodes/sevn/-/wikis/uploads/1985eb47f3bdc1a984cd2daa1ecf8ffb/conda_sevn_env.txt}{\texttt{ conda\+\_\+sevn\+\_\+env}}, simple conda environment containing a C++ compiler and Cmake
\item \href{https://gitlab.com/sevncodes/sevn/-/wikis/uploads/531b124d3d696f30d5d5b932fc18aa94/conda_sevn_python_env.txt}{\texttt{ conda\+\_\+sevn\+\_\+python\+\_\+env\+\_\+\+V2.\+txt}}, this environment contains also a working Python installation (V 3.\+9) including some basic packges such as numpy, scipy, astropy, matplotlib, pands. Install this environment if you want to use the SEVNpy package.
\end{DoxyItemize}

In order to use them\+:
\begin{DoxyItemize}
\item Install conda (if not alredy done)\+: \href{https://conda.io/projects/conda/en/latest/user-guide}{\texttt{ https\+://conda.\+io/projects/conda/en/latest/user-\/guide}}
\item Create the new env\+: {\ttfamily conda create —-\/name \texorpdfstring{$<$}{<}env\+\_\+name\texorpdfstring{$>$}{>} —-\/file conda\+\_\+sevn\+\_\+env.\+txt} or {\ttfamily conda create —-\/name \texorpdfstring{$<$}{<}env\+\_\+name\texorpdfstring{$>$}{>} —-\/file conda\+\_\+sevn\+\_\+python\+\_\+env\+\_\+\+V2.\+txt}
\item Active the new env\+: {\ttfamily conda activate \texorpdfstring{$<$}{<}env\+\_\+name\texorpdfstring{$>$}{>}}
\item Install and use SEVN as usual
\end{DoxyItemize}

See the \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}} (in {\itshape resources}) for additional information\hypertarget{index_autotoc_md4}{}\doxysubsection{Installation}\label{index_autotoc_md4}
Cmake is used to compile SEVN, there are two options, using directly Cmake or using a compile script\hypertarget{index_autotoc_md5}{}\doxysubsubsection{Compile script}\label{index_autotoc_md5}

\begin{DoxyItemize}
\item Locate the \href{compile.sh}{\texttt{ compile script}}
\item Open it and edit the line {\ttfamily SEVN=\char`\"{}$<$\+Insert absolute SEVNpath$>$\char`\"{}} and replace the string with the absolute path to the SEVN folder
\item Make the script executable {\ttfamily chmod u+x compile.\+sh}
\item Execute the script {\ttfamily ./compile.sh}
\item It is possibile to compile SEVN in parallel using {\ttfamily ./compile.sh -\/j}. {\bfseries{Be Careful}}\+: this will speed up the compilation at the cost of a significant memory usage. If you machine has less than 4 GB of RAM or if you are running other memory consuming processes do not use this option.
\item The executables ({\itshape sevn\+B.\+x} and {\itshape sevn.\+x}) will be located in {\itshape build/exe}
\item The script has other runtime options, use ./compile.sh -\/h to list them
\end{DoxyItemize}\hypertarget{index_autotoc_md6}{}\doxysubsubsection{Cmake}\label{index_autotoc_md6}

\begin{DoxyItemize}
\item Create a build folder (removing and already present build folder) {\ttfamily rm -\/r build; mkdir build} ~\newline

\item Enter in the build folder and execute cmake {\ttfamily cd build; cmake ..}
\item run make {\ttfamily make}
\item It is possibile to compile SEVN in parallel using {\ttfamily make -\/j}. {\bfseries{Be Careful}}\+: this will speed up the compilation at the cost of a significant memory usage. If you machine has less than 4 GB of RAM or if you are running other memory consuming processes do not use this option.
\item The executables ({\itshape sevn\+B.\+x} and {\itshape sevn.\+x}) will be located in {\itshape build/exe}
\end{DoxyItemize}\hypertarget{index_autotoc_md7}{}\doxysubsection{Run}\label{index_autotoc_md7}
The SEVN compilation produces two executable {\itshape sevn.\+x} to simulate the single stellar evolution of a list of star, and {\itshape sevn\+B.\+x} to simulate the binary evolution of a list of binaries.

The two executable can be run with a list of runtime parameters. All of them are optional except for the parameter -\/list containing the path of the input file storing the systems to evolve, e.\+g.

{\ttfamily ./sevnB.x -\/list ../../list\+Bin.dat}

The available runtime parameters are described in the \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}}\hypertarget{index_autotoc_md8}{}\doxysubsubsection{Run scripts}\label{index_autotoc_md8}
Writing a long list of runtime parameters can be tedious. For this reason we provide two run script template that can be used to quickly set a SEVN run. The two scripts are located in the \mbox{[}run\+\_\+scripts folder\mbox{]}(run\+\_\+scripts)\+:


\begin{DoxyItemize}
\item \href{run_scripts/run_sse.sh}{\texttt{ run\+\_\+sse.\+sh}} to simulate the single stellar evolution of a list of star
\item \href{run_scripts/run.sh}{\texttt{ run.\+sh}} to simulate the binary evolution of a list of binaries
\end{DoxyItemize}

In order to use the scripts\+:


\begin{DoxyItemize}
\item Open the script and edit the line {\ttfamily SEVN=\char`\"{}$<$\+Insert absolute SEVNpath$>$\char`\"{}} replacing the string with the absolute path to the SEVN folder
\item Edit the line {\ttfamily LISTBIN=\char`\"{}\$\{\+SEVN\}/run\+\_\+scripts/list\+Bin.\+dat\char`\"{}} replacing the string \char`\"{}\$\{\+SEVN\}/run\+\_\+scripts/list\+Bin.\+dat\char`\"{} with the path to the initial condition file
\item The script already contains all the runtime options. All of the are set to their default values.
\item Make the script executable {\ttfamily chmod u+x compile.\+sh}
\item Run the script {\ttfamily ./run.sh} or {\ttfamily ./run\+\_\+sse.sh}
\end{DoxyItemize}\hypertarget{index_autotoc_md9}{}\doxysection{Documentation}\label{index_autotoc_md9}
The SEVN \href{resources/SEVN_userguide.pdf}{\texttt{ user guide}} contains a general overview of SEVN, information on compilation and running, information on the runtime parameters and stellar tables, decription of the SEVN outputs and some hints and tips to analyse them.

A more \href{doc/doxyfiles/html/index.html}{\texttt{ technical documentation}} on the code and its components has been generated with Doxygen and can be found in the \href{doc/doxyfiles}{\texttt{ doc/doxyfiles}} folder.\hypertarget{index_autotoc_md10}{}\doxysection{Support}\label{index_autotoc_md10}
If you have problem in running SEVN or you find some weird behaviour/bugs please open an issue on the Gitlab repository. Please use the Gitlab issue tracker also to ask for requiring additional features for the next SEVN versions.

For any other questions/doubts/contributions send an email to\+: \href{mailto:giuliano.iorio.astro@gmail.com}{\texttt{ giuliano.\+iorio.\+astro@gmail.\+com}} and/or \href{mailto:sevnpeople@gmail.com}{\texttt{ sevnpeople@gmail.\+com}}\hypertarget{index_autotoc_md11}{}\doxysection{Contributing}\label{index_autotoc_md11}
SEVN is a general-\/purpose population-\/synthesis code and our idea is to create a network of users/developers. So, any contributions is highly encouraged. If you think it could be useful to add new processes/features in SEVN or you need it for a scientific project, please open an issue on Gitlab or contact the main SEVN team (see Support section).

We are working to create a SEVN-\/guide for developers that will help to directly modify and extend the code. The ideal way to contribute to the code is to fork it in your gitlab account and then send a merge-\/request when your version is ready to be included in the main SEVN repository. Please, contact us (see Support section) for any doubts.\hypertarget{index_autotoc_md12}{}\doxysection{Authors and acknowledgment}\label{index_autotoc_md12}
The original version of SEVN was developed by Mario Spera, Michela Mapelli and Alessandro Alberto Trani.

The current updated version is developed and maintained by Giuliano Iorio (main developer). The SEVN core team includes Guglielmo Costa, Gaston Escobar, Erika Korb, Michela Mapelli, Mario Spera, Cecilia Sgalletta.

The developers thank all the people in the DEMOBLACK group for all the valuable comments and suggestions during the code development.\hypertarget{index_autotoc_md13}{}\doxysection{License}\label{index_autotoc_md13}
MIT License

Copyright (c) 2022 Giuliano Iorio, Michela Mapelli, Mario Spera, Guglielmo Costa

See file LICENSE in the repository 