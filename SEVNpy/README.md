SEVNpy
=======

**SEVNpy** is a Python  library companion of the code SEVN ([https://gitlab.com/sevncodes/sevn](https://gitlab.com/sevncodes/sevn)). 
 
## Documentation
Read the documentation at [http://sevn.rtfd.io/](http://sevn.rtfd.io/)