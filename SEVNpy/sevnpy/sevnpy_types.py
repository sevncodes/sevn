from typing import Union, Type, List, Optional, Any, Dict, Tuple, Set

Number = Union[float, int]
ListLikeType = Union[List, Tuple, Set]
StandardValue = Union[int, float, str, bool]
